package ru.Voloshinskii.Sorter;


import com.sun.org.apache.xpath.internal.SourceTree;

public class Sorter implements Runnable{
    public static final String ASCENDING = "a";
    public static final String DESCENDING = "d";
    private String folderIn;
    private String name;
    private String prefix;
    private String typeSort;
    private String dataType;

    public Sorter(String folderIn, String name, String prefix, String typeSort, String dataType) {
        this.folderIn = folderIn;
        this.name = name;
        this.prefix = prefix;
        this.typeSort = typeSort;
        this.dataType = dataType;
    }
    public Sorter(String[] args, String fileName){
        this.folderIn = args[0];
        if(args[1].startsWith("--out-prefix=")) {
            this.prefix = args[1].substring(args[1].indexOf("=")+1);
        }
        if(args[2].startsWith("--content-type=")) {
            this.dataType = args[2].substring(args[2].indexOf("=")+1);
        }
        if(args[3].startsWith("--sort-mode=")) {
            this.typeSort = args[3].substring(args[3].indexOf("=")+1);
        }
        this.name = fileName;
    }


    public static String[] getSortedList(String[] mass, String sort){
        String[] result = new String[mass.length];
        result[0] = mass[0];
        boolean elementAdded = false;
        int lastId = 0;
            for(int i = 1; i < mass.length; i++){
                for(int j = 0; j <= lastId; j++){
                    int shortestWord = mass[i].getBytes().length <= result[j].getBytes().length ? mass[i].getBytes().length : result[j].getBytes().length;
                    for(int k = 0; k < shortestWord; k++){
                        if(compare(mass[i].getBytes()[k],result[j].getBytes()[k],sort))
                            break;
                        if(compare(result[j].getBytes()[k],mass[i].getBytes()[k],sort)){
                            elementAdded = true;
                            break;
                        }
                        if(mass[i].getBytes()[k] == result[j].getBytes()[k] && k == mass[i].getBytes().length - 1){
                            elementAdded = true;
                        }
                    }
                    if (elementAdded) {
                        shiftRight(result, j, lastId);
                        result[j] = mass[i];
                        lastId++;
                        elementAdded = false;
                        break;
                    }
                    if(j == lastId){
                        result[lastId+1] = mass[i];
                        lastId++;
                        break;
                    }
                }
            }
        
        
        return result;
    }

    private static boolean compare(Integer x, Integer y, String sort){
		if (sort.equals(ASCENDING))
			return x > y;
		else
			return x<y;
	}
	private static boolean compareOrEquals(Integer x, Integer y, String sort){
		if (sort.equals(ASCENDING))
			return x >= y;
		else
			return x <= y;
	}
	private static boolean compare(Byte x, Byte y, String sort){
		if (sort.equals(ASCENDING))
			return x > y;
		else
			return x<y;
	}
	private static boolean compareOrEquals(Byte x, Byte y, String sort){
		if (sort.equals(ASCENDING))
			return x >= y;
		else
			return x <= y;
	}

    
    private static <T> T[] shiftRight(T[] result, int indexLeft, int indexRight){
        if (indexRight>=0 && indexRight<result.length && indexLeft>=0 && indexLeft<result.length){
            for(int j = indexRight; j >= indexLeft; j--)
                result[j+1] = result[j];
        }
        return result;
    }


    private static Integer[] convertToIntegers(String[] mass)throws NumberFormatException{
        Integer[] result = new Integer[mass.length];
        for(int i = 0; i<mass.length;i++){
            result[i] = Integer.valueOf(mass[i]);
        }
        return result;
    }


    private static String[] convertToString(Integer[] mass){
        String[] result = new String[mass.length];
        for(int i = 0; i<mass.length;i++){
            result[i] = String.valueOf(mass[i]);
        }
        return result;
    }


    public static Integer[] getSortedList(Integer[] mass, String sort){
        Integer[] result = new Integer[mass.length];
            int lastId = 0;
            result[0] = mass[0];
            for (int i = 1; i< mass.length; i++){
                if (compare(mass[i],result[lastId],sort)){
                    lastId++;
                    result[lastId] = mass[i];
                }else{
                    for(int j = 0; j <= lastId; j++){
                        if (compareOrEquals(result[j],mass[i],sort)){
                            shiftRight(result, j, lastId);
                            result[j] = mass[i];
                            lastId++;
                            break;
                        }
                    }
                }
            }
        
 
        return result;
    }


    @Override
    public void run() {
        String[] wroteFile = FileRW.read(folderIn +"\\"+name);
        if (dataType.equals("i")){
            try {
                Integer[] list = Sorter.getSortedList(convertToIntegers(wroteFile), typeSort);
                FileRW.write(convertToString(list), folderIn, prefix, name);
            }catch (NumberFormatException e){
                System.out.println("Incorrect data file " + name);
                System.out.println("The file will not be sorted");
            }
        }else{
            String[] list = Sorter.getSortedList(wroteFile, typeSort);
            FileRW.write(list, folderIn, prefix, name);
        }
    }



}
