package ru.Voloshinskii.Sorter;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {

    static boolean exampleRegEx(String arg){
        Pattern p = Pattern.compile("--sort-mode=(a|d)");
        Matcher m = p.matcher(arg);
        return m.matches();
    }
    public static void main(String[] args) {
        System.out.println(exampleRegEx("--sort-mode= d"));
        checkParameters(args);
        Long timeStart = System.currentTimeMillis();
        Long timeFinish;
        File folderIn = new File(args[0]);
        final ExecutorService es = Executors.newFixedThreadPool(10);
        int size = folderIn.listFiles().length;
        Future[] future = new Future[size];
        for(int i = 0; i < size; i++){
            if(folderIn.listFiles()[i].canRead() && folderIn.listFiles()[i].isFile()){
//                future[i] = es.submit(new Sorter(IN,folderIn.listFiles()[i].getName(), "sorted_","d","s"));
                future[i] = es.submit(new Sorter(args, folderIn.listFiles()[i].getName()));
            }else{
                System.out.println("File \"" + folderIn.listFiles()[i].getName() + "\" can not be read");
            }
        }
        for(Future f: future){
            try {
                if (f != null)f.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        timeFinish = System.currentTimeMillis();
        System.out.println("Time: " + (timeFinish - timeStart));
        System.exit(0);
    }

    static void checkParameters(String[] args) {
        System.out.println("check arguments");
        int fault = 0;
        if (args == null || args.length < 3) {
            fault = 1;
        }else if (args[0] == null){
            fault = 2;
        }else if (!(new File(args[0]).exists())) {
            fault = 3;
        }else if (args[1] == null  || !checPrefix(args[1])) {
            fault = 4;
        }else if (args[2] == null || !checContentType(args[2])) {
            fault = 5;
        }else if (args[3] == null || !checSortMode(args[3])) {
            fault = 6;
        }
        switch (fault) {
            case 1:
                System.out.println("Set correct all parameters");
                break;
            case 2:
                System.out.println("Set correct directory");
                break;
            case 3:
                System.out.println("Directory does not exist");
                break;
            case 4:
                System.out.println("Set correct prefix");
                break;
            case 5:
                System.out.println("Set correct content type");
                break;
            case 6:
                System.out.println("No sort mode. Set sort mode");
                break;
        }
        if (fault != 0){
            System.out.println("The program can not be executed");
            System.exit(fault);
        }
        System.out.println("check is ok");
    }

    static boolean checPrefix(String arg){
        Pattern p = Pattern.compile("--out-prefix=\\S*");
        Matcher m = p.matcher(arg);
        return m.matches();
    }
    static boolean checContentType(String arg){
        Pattern p = Pattern.compile("--content-type=(i|s)");
        Matcher m = p.matcher(arg);
        return m.matches();
    }
    static boolean checSortMode(String arg){
        Pattern p = Pattern.compile("--sort-mode=(a|d)");
        Matcher m = p.matcher(arg);
        return m.matches();
    }


}
