package ru.Voloshinskii.Sorter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileRW {
    public static String[] read(String name){
        Scanner scn = null;
        try {
            scn = new Scanner(new File(name));
            List<String> list = new ArrayList<String>();
            while(scn.hasNext()){
                list.add(scn.nextLine());
            }
            String mass[] = list.toArray(new String[list.size()]);
            scn.close();
            return mass;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void write(String[] list, String folderName, String prefix, String name){
        System.out.println("Run create output file");
        String fileName = prefix + name;
        File folder = new File(folderName + "\\"+prefix);
        if (!folder.exists())folder.mkdirs();
        if (folder.exists()){
            String filePath = folder.getAbsolutePath() +"\\"+ fileName;
            File file = new File(filePath);
            System.out.println("create file: " + file.getAbsolutePath());
            FileWriter fw = null;
            try {
                fw = new FileWriter(file);
                for (String next:list) {
//                    if(next != null){
                        fw.append(next);
                        fw.append("\r\n");
                        fw.flush();
//                    }
                }
                System.out.println("File created");
            } catch (IOException e) {
                e.printStackTrace();
            }finally{
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else System.out.println("Folder is not created!");
    }

}
